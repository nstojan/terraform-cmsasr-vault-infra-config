# Policies

# ---------------------------------
# 1 #
# ---------------------------------

resource "vault_policy" "admin" {
  name = "admin"

  policy = <<EOT
# Mount secrets engines
path "sys/mounts/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# Configure the database secrets engine and create roles
path "database/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# Manage the leases
path "sys/leases/+/database/creds/deptrackadmin/*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}

path "sys/leases/+/database/creds/deptrackadmin" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}

# Write ACL policies
path "sys/policies/acl/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

# Manage tokens for verification
path "auth/token/create" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}
EOT
}

# ---------------------------------
# 2 #
# ---------------------------------

resource "vault_policy" "deptrack" {
  name = "deptrackadmin-policy"

  policy = <<EOT
path "database/creds/deptrackadmin" {
  capabilities = [ "read" ]
}
EOT
}

# Created two policies
# - admin
# - deptrackadmin-policy

# *********************************
# Database configuration 
# *********************************

# ---------------------------------
# 3 # enable database
# ---------------------------------

resource "vault_mount" "db" {
  path = "database"
  type = "database"
}

# ---------------------------------
# 4 #  
# ---------------------------------

resource "vault_database_secret_backend_role" "role" {
  backend             = vault_mount.db.path
  name                = "deptrackadmin"
  db_name             = vault_database_secret_backend_connection.postgres.name
  creation_statements = [
    "CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}' INHERIT;",
    "GRANT deptrackadmin TO \"{{name}}\";"
  ]
}

# ---------------------------------
# 5 #
# ---------------------------------

resource "vault_database_secret_backend_connection" "postgres" {
  backend       = vault_mount.db.path
  name          = "deptrack"
  allowed_roles = ["deptrackadmin"]   # name to the 'role' in 4

  postgresql {
    connection_url = "postgres://${var.postgres_user}:${var.postgres_password}@pg-postgresql.pg.svc.cluster.local:5432/postgres?sslmode=disable"
  }
}

# Kubernetes Config
# ---------------------------------
# 6 # 
# ---------------------------------

resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"
}

# ---------------------------------
# 7 #
# ---------------------------------

resource "vault_kubernetes_auth_backend_config" "zremal" {
  backend                = vault_auth_backend.kubernetes.path
  kubernetes_host        = "https://kubernetes.default.svc.cluster.local:443"
}

# ---------------------------------
# 8 #
# ---------------------------------

resource "vault_kubernetes_auth_backend_role" "deptrack" {
  backend                          = vault_auth_backend.kubernetes.path
  role_name                        = "deptrackadmin"  # role created 
  bound_service_account_names      = ["postgres"]
  bound_service_account_namespaces = ["post"]
  token_ttl                        = 3600
  token_policies                   = ["deptrackadmin-policy"]
}