variable "postgres_user" {
  type      = string
  default   = "postgres"
  sensitive = true
}

variable "postgres_password" {
  type      = string
  sensitive = true
}

variable "vault_token" {
  type      = string
  sensitive = true
}

variable "vault_addr" {
  default = "http://vault.k3s.zremal.org:8080/"
}